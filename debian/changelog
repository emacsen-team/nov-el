nov-el (0.4.0-3) UNRELEASED; urgency=medium

  [ Nicholas D Steeves ]
  * Drop 0001-drop-unnecessary-esxml-0.3.6-requirement-0.3.5-is-fi.patch,
    which is not needed in Debian 12 (bullseye).

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster:
    + elpa-nov: Drop versioned constraint on emacs in Recommends.

 -- Nicholas D Steeves <sten@debian.org>  Sat, 09 Apr 2022 19:24:54 -0400

nov-el (0.4.0-2) unstable; urgency=medium

  * Team upload.
  * Rebuild against dh-elpa 2.1.5.
    Upload pushed to dgit-repos but not salsa.
    See <https://lists.debian.org/debian-emacsen/2024/07/msg00077.html>.

 -- Sean Whitton <spwhitton@spwhitton.name>  Thu, 25 Jul 2024 22:59:57 +0900

nov-el (0.4.0-1) unstable; urgency=medium

  * New upstream release.
  * Rebase patch queue onto this release.

 -- Nicholas D Steeves <sten@debian.org>  Fri, 08 Apr 2022 22:33:30 -0400

nov-el (0.3.4-1) unstable; urgency=medium

  * Declare Standards-Version: 4.6.0 (no changes required).
  * Upload to unstable.

 -- Nicholas D Steeves <sten@debian.org>  Wed, 15 Sep 2021 17:46:34 -0400

nov-el (0.3.4-1~exp1) experimental; urgency=low

  * New upstream release (Closes: #985913).
  * control and copyright: Update my email address.
  * Update Homepage, Source, and watch file with new upstream location.
  * Install new upstream README.md (replaces README.rst).
  * Migrate to debhelper-compat 13.
  * Assert Rules-Requires-Root: no.
  * Add 0001-drop-unnecessary-esxml-0.3.6-requirement-0.3.5-is-fi.patch.

 -- Nicholas D Steeves <sten@debian.org>  Fri, 26 Mar 2021 06:02:11 -0400

nov-el (0.3.0-1) unstable; urgency=medium

  * New upstream release.
  * Add myself to copyright file.
  * Declare Standards-Version: 4.5.0. (no changes required)

 -- Nicholas D Steeves <nsteeves@gmail.com>  Sun, 03 May 2020 21:50:33 -0400

nov-el (0.2.9-1) unstable; urgency=medium

  * New upstream release.

 -- Nicholas D Steeves <nsteeves@gmail.com>  Wed, 21 Aug 2019 16:13:18 -0400

nov-el (0.2.8-2) unstable; urgency=medium

  * Add autoload cookie using d/debian-autoloads.el, installed using d/elpa;
    This activates nov-mode when visiting .epub files.  We're using a
    Debian-specific method because upstream NACKed it here:
      https://github.com/wasamasa/nov.el/issues/42
    The three Emacs modes for reading EPUBs are this package, Jayson
    William's epub-mode, which is only available on emacswiki.org and thus
    won't be packaged for Debian, and Ben Dean's ereader.el, which won't be
    packaged because it's unmaintained.
  * This package doesn't use pristine-tar, so disable it in d/gbp.conf.
  * Switch to debhelper-compat 12 and drop debian/compat.
  * Drop emacs24 and emacs25 from Enhances, because they do not exist in
    buster (eg: not even useful for backports).
  * Add myself to Uploaders.
  * Add elpa-olivetti to Suggests.  This mode may enable a faster and/or more
    comfortable reading experience; Activate it with 'M-x olivetti-mode'.
    Olivetti-mode will lay out the ebook body using shorter lines, and will
    centre this text in the larger frame.  The effect is similar to having
    a portrait-oriented A5 sheet filled with text centred inside of a blank
    landscape-oriented A4 sheet.
  * Uncapitalise short description and mention Emacs here and in the first
    line of the long description; also mention "ebooks" to make this package
    more discoverable.
  * Declare Standards-Version: 4.4.0. (no further changes required)

 -- Nicholas D Steeves <nsteeves@gmail.com>  Tue, 23 Jul 2019 23:14:27 -0400

nov-el (0.2.8-1) unstable; urgency=medium

  * New upstream release.
  * Update copyright years.

 -- Sean Whitton <spwhitton@spwhitton.name>  Sun, 14 Jul 2019 16:14:49 +0100

nov-el (0.2.7-1) unstable; urgency=medium

  * New upstream release.

 -- Sean Whitton <spwhitton@spwhitton.name>  Sat, 12 Jan 2019 09:54:13 -0700

nov-el (0.2.5-1) unstable; urgency=medium

  * New upstream release.

 -- Sean Whitton <spwhitton@spwhitton.name>  Sat, 20 Oct 2018 12:38:03 -0700

nov-el (0.2.4-1) unstable; urgency=medium

  * New upstream release.
  * Fix typo in Vcs-Git.
  * Bump copyright years.
  * Update Maintainer field for team rename.

 -- Sean Whitton <spwhitton@spwhitton.name>  Sat, 23 Jun 2018 08:59:30 +0100

nov-el (0.2.3-1) unstable; urgency=medium

  * New upstream release.
  * Point Vcs-* to salsa.

 -- Sean Whitton <spwhitton@spwhitton.name>  Thu, 29 Mar 2018 10:37:05 -0700

nov-el (0.2.2-1) unstable; urgency=medium

  * New upstream release.

 -- Sean Whitton <spwhitton@spwhitton.name>  Sun, 21 Jan 2018 09:29:26 -0700

nov-el (0.2.1-1) unstable; urgency=medium

  * New upstream release.

 -- Sean Whitton <spwhitton@spwhitton.name>  Wed, 27 Sep 2017 16:08:38 -0700

nov-el (0.2.0-1) unstable; urgency=medium

  * Initial release (Closes: #876322).

 -- Sean Whitton <spwhitton@spwhitton.name>  Sat, 23 Sep 2017 13:12:24 -0700
